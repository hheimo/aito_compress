from http.server import BaseHTTPRequestHandler, HTTPServer


# Calculate the length of common prefix
def len_common_prefix(previous, current):
    prefix = ''

    len_previous = len(previous)
    for i in range(len_previous):
        if(previous[i] != current[i]):
            return len(prefix)
        else:
            prefix += previous[i]
    return len(prefix)


def compress(words):
    previous = ''
    encoded = []

    for current in words:
        # Calculate the number of common letters between previous and current
        len_prefix = len_common_prefix(previous, current)

        encoded.append(str(len_prefix)+" "+current[len_prefix:])
        previous = current

    return encoded


def decompress(encoded):
    previous = ""
    words = []

    for current in encoded:

        encoding_obj = current.split()
        # Get length of the prefix and cast to int
        len_prefix = int(encoding_obj[0])
        if(len_prefix == 0):
            word = encoding_obj[1]
            words.append(word)
        else:
            word = previous[:len_prefix]+encoding_obj[1]
            words.append(word)

        previous = word

    return words


class HandleRequests(BaseHTTPRequestHandler):
    def _set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/plain')
        self.end_headers()

    def _set_bad_request(self):
        self.send_response(400)
        self.send_header('Content-type', 'text/plain')
        self.end_headers()

    def do_POST(self):
        '''Reads post request body'''
        content_length = int(
            self.headers['Content-Length'])  # <--- Gets the size of data
        post_data = self.rfile.read(content_length).decode(
            'utf-8')  # <--- Gets the data itself

        post_data = post_data.split('\n')

        # trim leading slash from path
        path = self.path[1:]
        if path == "compress":
            words = compress(post_data)
            result = '\n'.join(map(str, words))
            self._set_headers()
            self.wfile.write(result.encode('utf-8'))
        elif path == "decompress":
            words = decompress(post_data)
            result = '\n'.join(map(str, words))
            self._set_headers()
            self.wfile.write(result.encode('utf-8'))
        else:
            self._set_bad_request()


host = ''
port = 8080
print("Starting server at localhost with port 8080")
HTTPServer((host, port), HandleRequests).serve_forever()

9955
