# Lossless String Compression

Programming assignment for Aito AI

You can use words_alpha.txt for testing.

## Testing environment

- Ubuntu 16.04 LTS
- Python == 3.7.0

## Usage

```bash
git clone https://gitlab.com/hheimo/aito_compress.git
cd aito_compress
python server.py
```

### Compress

```bash
curl -XPOST --data-binary @file_to_compress.txt http://localhost:8080/compress > target_file.txt

```

### De-compress

```bash
curl -XPOST --data-binary @file_to_decompress.txt http://localhost:8080/decompress > target_file.txt

```
